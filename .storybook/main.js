module.exports = {
  stories: ['../stories/**/*.stories.mdx', '../stories/**/*.stories.@(js|jsx|ts|tsx|svelte)'],
  addons: ['@storybook/addon-svelte-csf', '@storybook/addon-links', '@storybook/addon-essentials'],
  framework: '@storybook/svelte',
  svelteOptions: {
    preprocess: require('../svelte.config.js').preprocess,
  },
}
