import { create } from '@storybook/theming'
export default create({
  base: 'dark',
  appBorderRadius: 8,
  brandTitle: 'CosmikUI',
  appBg: '#222',
  appContentBg: '#222',
  barBg: '#222',
  colorPrimary: '#ddd',
  colorSecondary: '#4117ff',
  barSelectedColor: '#ccc',
  inputBg: '#441ac4',
})
