# Cosmik : Svelte UI Components Library

**[Cosmik](https://www.npmjs.com/package/cosmik)** is Out-of-the-box [Svelte]() UI components, styles, configurations library to provide user and devloper friendly interfaces for you web applications.

## Installation

```js
//For using yarn
yarn add -D cosmik

//For using npm
npm install -D cosmik
```

## Usages

**Cosmik** provides multiple API to call for library like...

- `cosmik/core` provides components, widgets, complex components.
- `cosmik/styles` provide stylesheets in scss, theme and more.
