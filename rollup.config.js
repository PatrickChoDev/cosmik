import autoPreprocess from 'svelte-preprocess'
import pkg from './package.json'
import resolve from '@rollup/plugin-node-resolve'
import svelte from 'rollup-plugin-svelte'
import typescript from '@rollup/plugin-typescript'
import commonjs from '@rollup/plugin-commonjs'
import css from 'rollup-plugin-css-only'

const name = pkg.name
  .replace(/^(@\S+\/)?(svelte-)?(\S+)/, '$3')
  .replace(/^\w/, (m) => m.toUpperCase())
  .replace(/-\w/g, (m) => m[1].toUpperCase())

export default {
  input: 'components/index.ts',
  output: [
    { file: pkg.module, format: 'es', name, sourcemap: true },
    { file: pkg.main, format: 'umd', name, sourcemap: true },
  ],
  plugins: [
    svelte({
      preprocess: autoPreprocess({
      }),
      onwarn: (warning, handler) => {
        if (warning.code === 'a11y-distracting-elements') return
        handler(warning)
      },
    }),
    css({ output: 'bundle.css' }),
    resolve({
      browser: true,
      dedupe: ['svelte'],
    }),
    commonjs(),
    typescript({ ...require('./tsconfig.json').compilerOptions }),
  ],
}
