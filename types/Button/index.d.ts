/// <reference types="svelte" />

import { SvelteComponentTyped } from 'svelte'

export interface ButtonProps
  extends svelte.JSX.HTMLAttributes<HTMLElementTagNameMap['button']>,
    svelte.JSX.HTMLAttributes<HTMLElementTagNameMap['a']>,
    svelte.JSX.HTMLAttributes<HTMLElementTagNameMap['div']> {
  /**
   * @default (e:any) => {}
   */
  call?: () => void

  /**
   * @default "default"
   */
  size?: 'default' | 'field' | 'small' | 'lg' | 'xl'

  /**
   * @default ""
   */
  style?: string | [string]

  /**
   * @default 0
   */
  disableOnClickedTimeout?: number

  /**
   * @default false
   */
  clickedLoading: boolean
}

export default class Button extends SvelteComponentTyped<
  ButtonProps,
  {
    click: WindowEventMap['click']
    mouseover: WindowEventMap['mouseover']
    mouseenter: WindowEventMap['mouseenter']
    mouseleave: WindowEventMap['mouseleave']
  },
  {
    default: {
      props: {
        call?: () => void
        size?: string
        style?: string | [string]
        disableClickedOnTimeout?: number
        clickedLoading?: boolean
      }
    }
  }
> {}
