/// <reference types="svelte" />

import { SvelteComponentTyped } from 'svelte'

export interface LinkProps
  extends svelte.JSX.HTMLAttributes<HTMLElementTagNameMap['a']>,
    svelte.JSX.HTMLAttributes<HTMLElementTagNameMap['div']> {
  /**
   * @default "primary"
   */
  variant?: 'primary' | 'secondary' | 'ghost'
  /**
   * @default "#"
   */
  href: string | any

  /**
   * @default null
   */
  tooltip?: string | boolean
}

export default class Link extends SvelteComponentTyped<
  LinkProps,
  {
    click: WindowEventMap['click']
    mouseover: WindowEventMap['mouseover']
    mouseenter: WindowEventMap['mouseenter']
    mouseleave: WindowEventMap['mouseleave']
  },
  {
    default: {
      props: {
        variant?: 'primary' | 'secondary' | 'ghost'
        href: string | any
        tooltip?: string | boolean
      }
    }
  }
> {}
